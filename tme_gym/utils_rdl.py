import torch
from torch import nn
import torch.nn.functional as F
from collections import deque
from random import sample
import numpy as np

from gym import wrappers, logger

from tensorflow.python.summary.summary_iterator import summary_iterator
import os



#################################################################################
## Files reading
#################################################################################

def read_runs_dir(path, keys):
    out = {}
    for dir,_,files in os.walk(path):
        for file in files:
            if "events.out.tfevents" in file:
                f = os.path.join(dir, file)
                out[f] = read_tensorboard_simple_value(f, keys)
    return out

def read_tensorboard_simple_value(path, keys):
    val_dict = dict([(k,([],[])) for k in keys])
    for e in summary_iterator(path):
        for v in e.summary.value:
            if v.tag in keys:
                val_dict[v.tag][0].append(e.step)
                val_dict[v.tag][1].append(v.simple_value)
    for k,(idx,v) in val_dict.items():
        val_dict[k] = np.array(idx), np.array(v)
    return val_dict



#################################################################################
## Modules
#################################################################################

class NN(nn.Module):
    def __init__(self, inSize, outSize, layers=[], activ='leaky_relu', input_batchNorm=False, batchNorm=False):
        """ activ in 'leaky_relu', 'tanh'
        """
        super(NN, self).__init__()
        self.layers = nn.ModuleList([])
        self.input_batchNorm = nn.BatchNorm1d(inSize) if input_batchNorm or batchNorm else None
        self.batchNorm_layers = nn.ModuleList([]) if batchNorm else None
        for x in layers:
            if batchNorm:
                self.batchNorm_layers.append(nn.BatchNorm1d(inSize))
            self.layers.append(nn.Linear(inSize, x))
            inSize = x
        self.layers.append(nn.Linear(inSize, outSize))
        if activ=='leaky_relu':
            self.activ = F.leaky_relu
        elif activ=='tanh':
            self.activ = torch.tanh
        else:
            raise ValueError
    def forward(self, x):
        if self.input_batchNorm is not None:
            x = self.input_batchNorm(x)
        x = self.layers[0](x)
        for i in range(1, len(self.layers)):
            x = self.activ(x)
            if self.batchNorm_layers is not None:
                x= self.batchNorm_layers[i-1](x)
            x = self.layers[i](x)
        return x

class MultiInputNN(nn.Module):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.fc = NN(*args, **kwargs)
    def forward(self, *args):
        return self.fc(torch.cat(args, dim=-1))



#################################################################################
## env methods
#################################################################################

class BaseEnvMethods:
    env_name = None
    def __init__(self,verbose=True):
        self.verbose = verbose
        if verbose:
            print("env_name = '{}'".format(self.env_name))
    def get_env_name(self):
        if self.env_name is None:
            raise NotImplementedError
        return self.env_name
    def Phi(self, obs):
        raise NotImplementedError

class GridWorldMethods(BaseEnvMethods):
    env_name = 'gridworld-v0'
    def Phi(self, obs):
        return obs.dumps()

class FeaturesExtractor(object):
    def __init__(self,outSize):
        super().__init__()
        self.outSize=outSize*3
    def getFeatures(self, obs):
        state=np.zeros((3,np.shape(obs)[0],np.shape(obs)[1]), dtype=np.float32)
        state[0]=np.where(obs == 2,1,state[0])
        state[1] = np.where(obs == 4, 1, state[1])
        state[2] = np.where(obs == 6, 1, state[2])
        return state.reshape(-1)
class GridWorldFeaturesMethods(BaseEnvMethods):
    env_name = 'gridworld-v0'
    def __init__(self, feature_size=2, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.feature_extractor = FeaturesExtractor(feature_size)
    def Phi(self, obs):
        return self.feature_extractor.getFeatures(obs)


class CartPoleDiscreteMethods(BaseEnvMethods):
    env_name = 'CartPole-v1'
    def Phi(self, obs):
        return obs.astype('float32')
class LunarLanderDiscreteMethods(BaseEnvMethods):
    env_name = 'LunarLander-v2'
    def Phi(self, obs):
        return obs.astype('float32')

class BaseContinuousEnvMethods(BaseEnvMethods):
    min_action = None
    max_action = None
    action_dim = None
    def get_action_range(self):
        if self.min_action is None or self.max_action is None:
            raise NotImplementedError
        return (self.min_action, self.max_action)
    def get_action_dim(self):
        if self.action_dim is None:
            raise NotImplementedError
        return self.action_dim

class MountainCarContinuousMethods(BaseContinuousEnvMethods):
    min_action = -1
    max_action = 1.
    action_dim = 1
    env_name = 'MountainCarContinuous-v0'
    def Phi(self, obs):
        return np.array(obs, dtype=np.float32)
class LunarLanderContinuousMethods(BaseContinuousEnvMethods):
    min_action = -1.
    max_action = 1.
    action_dim = 2
    env_name = 'LunarLanderContinuous-v2'
    def Phi(self, obs):
        return np.array(obs, dtype=np.float32)
class PendulumContinuousMethods(BaseContinuousEnvMethods):
    min_action = -2.
    max_action = 2.
    action_dim = 1
    env_name = 'Pendulum-v0'
    def Phi(self, obs):
        return np.array(obs, dtype=np.float32)



#################################################################################
## Agents
#################################################################################

class AleaStock():
    def __init__(self, mem_size):
        self.mem_size = mem_size
        self.memory = deque(maxlen=mem_size)
    def __len__(self):
        return len(self.memory)
    def add(self, data):
        self.memory.append(data)
    def sample(self, n):
        if n>len(self.memory):
            n = self.__len__()
        return sample(self.memory, n)

# class AleaStockTensor():
#     def __init__(self, mem_size, dtypes):
#         self.dtypes = dtypes
#         self.mem_size = mem_size
#         self.current_size = 0
#         self.memory = None
#     def add(self, data):
#         ## init memory :
#         if self.memory is None:
#             self.memory = []
#             for d,dtype in zip(data, self.dtypes):
#                 shape = [self.mem_size] + list(np.array(d).shape)
#                 self.memory.append(torch.zeros(shape, dtype=dtype))
#         ## add :
#         for m,d in zip(self.memory, data):
#             m = torch.cat((torch.tensor([d], dtype=m.dtype), m[:-1]))
#         if self.current_size < self.mem_size:
#             self.current_size += 1

#     def sample(self, n):
#         n = min(n, self.current_size)
#         idx = np.random.choice(self.current_size, n, replace=False)
#         return [m[idx] for m in self.memory]


class BaseAgent:
    def __init__(self, env_methods:BaseEnvMethods):
        self.env_methods = env_methods
        self.Phi = env_methods.Phi ## the obs to state method
        self.log_dict = {}
    def interface_act(self, obs):
        return self.act(self.Phi(obs))
    def act(self, state):
        """ method called by the playing procedure (play_one_run)
        """
        raise NotImplementedError
    def interface_act_explore(self, obs):
        return self.act_explore(self.Phi(obs))
    def act_explore(self, state):
        """ method called by the training procedure (train_agent)
        can be overriden if needed
        """
        return self.act(state)
    def interface_observe(self, obs1, action, reward, obs2, done):
        return self.observe(self.Phi(obs1), action, reward, self.Phi(obs2), done)
    def observe(self, state1, action, reward, state2, done):
        raise NotImplementedError

no_preprocess = lambda x:x
def tensor_preprocessing(batch):
    s, a, r, s2, d = zip(*batch)
    ts  = torch.tensor(s,  dtype=torch.float32)
    ts2 = torch.tensor(s2, dtype=torch.float32)
    ta = torch.tensor(a)
    tr = torch.tensor(r).float()
    td = torch.tensor(d)
    out = [ts, ta, tr, ts2, td]
    out = [t if len(t.shape)==2 else t.view(t.shape[0], -1) for t in out]
    return out


class BaseBatchEpisodeAgent(BaseAgent):
    """ stores trajectory and calls update function at the end of it """
    def __init__(self, preprocess_batch=tensor_preprocessing, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mem = []
        self.preprocess_batch = preprocess_batch
    def update(self, batch):
        s, a, r, s2, d = batch
        raise NotImplementedError
    def observe(self, s1, a, r, s2, done):
        self.mem.append((s1,a,r,s2,done))
        if done:
            batch = self.preprocess_batch(list(self.mem))
            self.mem = []
            self.update(batch)

class BaseMemAgent(BaseAgent):
    def __init__(self, env_methods:BaseEnvMethods, batch_size=100, mem_size=100000, update_freq=1, num_updates=1, min_mem_filling=None, preprocess_batch=tensor_preprocessing):
        super().__init__(env_methods=env_methods)
        self.mem = AleaStock(mem_size=mem_size)
        if min_mem_filling is None:
            min_mem_filling = 10*batch_size
        self.min_mem_filling = min_mem_filling
        self.update_freq = update_freq
        self.num_updates = num_updates
        self.last_update = update_freq
        self.batch_size = batch_size
        self.preprocess_batch = preprocess_batch
    def update(self, batch):
        s, a, r, s2, d = batch
        raise NotImplementedError
    def observe(self, state1, action, reward, state2, done):
        self.mem.add((state1, action, reward, state2, done))
        if len(self.mem) >= self.min_mem_filling and self.last_update >= self.update_freq:
            for _ in range(self.num_updates):
                batch = self.preprocess_batch(self.mem.sample(self.batch_size))
                self.update(batch)
            self.last_update = 0
        self.last_update += 1



#################################################################################
## Training
#################################################################################

def play_one_run(agent:BaseAgent, env, max_iter=500, verbose=1):
    """ make one run of the agent on the environment """
    envm = wrappers.Monitor(env, directory='directory', force=True, video_callable=False)
    obs = envm.reset()
    reward = 0
    done = False
    rsum = 0

    env.verbose = (verbose >= 2)  # afficher 1 episode sur 100
    j = 0
    while True:
        action = agent.interface_act(obs)
        obs2, reward, done, _ = envm.step(action)
        # agent.interface_observe(obs, action, reward, obs2, done)
        obs = obs2
        rsum += reward
        j += 1
        if verbose>=2:
            env.render(.5)
        if done or j>max_iter:
            break

    envm.close()
    if verbose>=1:
        print("num_iter={}\t{}\trsum={}".format(j, 'DONE    ' if done else 'MAX_ITER', rsum))
    return rsum, j, done


def train_agent(agent:BaseAgent, env, max_iter=500, verbose=1, num_episodes=100, num_iter=None):
    """ train the agent on the environment
    if num_episodes is not None, then num_iter is ignored
    """
    rsum_list, done_list, num_iter_list = [], [], []
    episode_count = 0
    total_iter = 0
    while True:
        obs = env.reset()
        rsum = 0
        j = 0

        reward = 0
        done = False
        while True:
            action = agent.interface_act_explore(obs)
            obs2, reward, done, _ = env.step(action)
            if j>=max_iter-1:
                done = True
            agent.interface_observe(obs, action, reward, obs2, done)
            obs = obs2
            rsum += reward
            j += 1
            total_iter += 1
            if done:
                break

        episode_count += 1

        if verbose>=1:
            print("Episode {}\tnum_iter={}\t{}\trsum={}".format(episode_count, j, 'DONE    ' if j<max_iter else 'MAX_ITER', rsum))
        rsum_list    .append((episode_count, rsum))
        num_iter_list.append((episode_count, j))
        done_list    .append((episode_count, done))

        if num_episodes is None and total_iter>=num_iter:
            break
        if num_episodes is not None and episode_count>=num_episodes:
            break

    out_dict = {"rsum":rsum_list, 'done':done_list, 'num_iter':num_iter_list}
    return out_dict

def sample_param(vmin, vmax, log=False, integer=False):
    v = random()
    if log:
        v = np.exp(v*(np.log(vmax/vmin)) + np.log(vmin))
    else:
        v = v*(vmax-vmin) + vmin
    if integer:
        v = int(v)
    return v



#################################################################################
## Noise
#################################################################################

class Orn_Uhlen:
    """ by Sylvain Lamprier """
    def __init__(self, n_actions, mu=0, theta=0.15, sigma=0.2):
        self.n_actions = n_actions
        self.X = np.ones(n_actions) * mu
        self.mu = mu
        self.sigma = sigma
        self.theta = theta

    def reset(self):
        self.X = np.ones(self.n_actions) * self.mu

    def sample(self):
        dX = self.theta * (self.mu - self.X)
        dX += self.sigma * np.random.randn(self.n_actions)
        self.X += dX
        return self.X

class OUNoise(object):
    """ Ornstein-Ulhenbeck Process
    from https://github.com/vitchyr/rlkit/blob/master/rlkit/exploration_strategies/ou_strategy.py
    """
    def __init__(self, action_space, mu=0.0, theta=0.15, max_sigma=0.3, min_sigma=0.3, decay_period=100000):
        self.mu           = mu
        self.theta        = theta
        self.sigma        = max_sigma
        self.max_sigma    = max_sigma
        self.min_sigma    = min_sigma
        self.decay_period = decay_period
        self.action_dim   = action_space.shape[0]
        self.low          = action_space.low
        self.high         = action_space.high
        self.reset()

    def reset(self):
        self.state = np.ones(self.action_dim) * self.mu

    def evolve_state(self):
        x  = self.state
        dx = self.theta * (self.mu - x) + self.sigma * np.random.randn(self.action_dim)
        self.state = x + dx
        return self.state

    def get_action(self, action, t=0):
        ou_state = self.evolve_state()
        self.sigma = self.max_sigma - (self.max_sigma - self.min_sigma) * min(1.0, t / self.decay_period)
        return np.clip(action + ou_state, self.low, self.high).astype(action.dtype)

class MultiCount:
    def __init__(self, num_counters):
        self.num_counters = num_counters
        self.reset()
    def add(self, vect):
        if len(vect) != self.num_counters:
            raise ValueError
        self.counters = self.counters + np.array(vect)
    def reset(self):
        self.counters = np.zeros(self.num_counters)
    def dict(self, pref=''):
        return dict([(pref+str(i), c) for i,c in enumerate(self.counters)])
    def list(self):
        return list(self.counters)



#################################################################################
## smoothing
#################################################################################

def smooth(scalars, weight):  # Weight between 0 and 1
    last = scalars[0]  # First value in the plot (first timestep)
    smoothed = list()
    for point in scalars:
        smoothed_val = last * weight + (1 - weight) * point  # Calculate smoothed value
        smoothed.append(smoothed_val)                        # Save it
        last = smoothed_val                                  # Anchor the last smoothed value

    return smoothed
