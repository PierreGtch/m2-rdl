<!-- Compile me with atom's markdown-preview-plus package -->

# Reinforcement Learning

DAC 2019/2020
*Pierre Guetschel*

## 1 - Bandits

Sur le problème des bandits, on observe que `LinUCB` semble optimal sur ce problème pour une valeur de alpha proche de 0.1 (courbe grise ci-dessous).

Pour alpha=1 (courbe violette), on constate que `LinUCB` performe moins bien que `UCB` lors des premières itérationsmais qu'il s'adapte et parvient à le dépasser par la suite.

Egalement on remarque que le meilleur `LinUCB` (alpha=0.1, courbe grise) dépasse `Static Best` (courbe orange), ce qui montre que les réponses de `LinUCB` dépendent nécessairement du contexte.\
![](report_images/1_cum_reward.png "Cumulative rewards")
![](report_images/1_cum_regret.png "Cumulative regrets")



## 2 - Value/Policy Iteration
**GridWorld**\
Sur les plans simples on constate que les 2 algorithmes convergent vers des solutions optimales et évidement que Policy iteration converge moins rapidement que Value iteration. Par exemple sur le plan 6, Value Iter onverge en 67 value_steps et Policy iter en 19 policy_steps (ce qui représente 1621 value_steps).

Sur les plans 7 et 4, l'agent doit passer par une case de récompense négative. On arrive à faire converger value iteration mais vers une solution légèrement sous-optimale avec un gamma et une précision requise élevés (en 1340 value_steps). Policy iteration lui ne converge pas en temps raisonnable.

En revanche sur le plan 9, impossible de charger le MDP: `maximum recursion depth exceeded while calling a Python object`.

Finalement on constate que lorsque l'agent a accès au MDP, le problème est simple mais les possibilités sont limités car la combinatoire explose rapidement.



## 3 - Q-Learning

***Remarque***: Dans cette section et les suivantes, on présentera les résultats comme ceci:  en haut, courbe noté `rsum`, l'historique des rewards cumulés obtenus lors de chacun des runs avec en abscisse le nombre d'episodes joués. En bas, courbe noté `num_iter`, le nombre d'itérations de chacun des runs.\
Pour CartPole, les deux courbes sont identiques car chaque action donne un reward de 1.

**GridWorld** (map 1)\
On constate une convergence relativement lente (environ 5000 épisodes) avec l'algorithme Q-Learning et nettement plus rapide avec sa variante SARSA. En revanche les 2 versions sont peu stable. On parvient à atteindre des solutions optimales mais on finit par les perdre.

Q-Learning  à gauche and SARSA à droite :\
![](report_images/3_QLearning_map1.png "3_QLearning_map1")
![](report_images/3_SARSA_map1.png "3_SARSA_map1")\
En revanche je n'ai pas réussi à faire converger DynaQ:\
![](report_images/3_DynaQ_map1.png "3_DynaQ_map1")



## 4 - DQN
**CartPole**\
Sur CartPole, on arrive à faire converger rapidement DQN (moins de 100 épisodes) mais vers une solution sous-optimale: le score maximal de 500pts est rarement atteint et on se stabilise sur une moyenne autour de 200pts.\
![](report_images/4_DQN_cartpole.png "4_DQN_cartpole")

**LunarLander**\
Sur lunarLander on observe des résultats très instables aussi. A certains runs l'agent obtiens un score positif supérieur à 200 et le run suivant -200. La moyenne des rewards se stabilise autour de 0.\
![](report_images/4_DQN_lunarlander.png "4_DQN_lunarlander")

Je n'ai pas réussi à faire converger DQN sur **GridWorld** carte 1, même si on observe quelques runs concluants :\
![](report_images/4_DQN_gridworld.png "4_DQN_gridworld")



## 5 - A2C
**CartPole**\
Je n'ai pas réussi à faire converger la version **Online A2C**.
On parvient obtenir des runs  de 50 itérations au début et en moyenne à 22 avec un learning rate faible. Mais on finit par observer une divergence et no tombe à 10 itérations.\
![](report_images/5_A2Conline_cartpole.png "5_A2Conline_cartpole")\
Avec la version **Batched A2C**, on parvient à obtenir le score maximal (500 itérations) mais l'algorithme reste relativement instable et on observe des rechutes.\
![](report_images/5_A2Cbatched_cartpole.png "5_A2Cbatched_cartpole")

**LunarLander**\
Seule la version **Batched A2C** a été testée sur lunarLander. On observe une très lente convergence vers une solution sous-optimale,
mais quand même on voit un nombre significatif de runs concluants (où la sonde s'est posée).\
![](report_images/5_A2Cbatched_lunarlander.png "5_A2Cbatched_lunarlander")

Je n'ai pas réussi à faire converger **Batched A2C** sur **GridWorld** :\
![](report_images/5_A2Cbatched_gridworld.png "5_A2Cbatched_gridworld")



## 6 - PPO KL/Clipped
Lors du premier passage de chacune des boucles d'optimisation, la divergence KL est nulle car les 2 distributions sont identiques

**CartPole**\
Avec la version **PPO Clipped**, on parvient à atteindre le score maximal mais l'algorithme est peu stable. Ci-dessous 2 entraînements successifs (droite et gauche) de PPO Clipped avec tous paramètres identiques. Seule les conditions initiales des simulations changent.
On observe des courbes d'apprentissages très différentes.
L'algorithme a atteint le score maximal (500) lors du premier entraînement (à gauche) mais pas lors du second (à droite):\
![](report_images/6_PPOclipped_cartpole_run1.png "6_PPOclipped_cartpole_run1")
![](report_images/6_PPOclipped_cartpole_run2.png "6_PPOclipped_cartpole_run2")\
Même remarque avec **PPO-KL**. Ici le score maximal a été atteint les deux fois mais pas en autant d'itérations:\
![](report_images/6_PPOKL_cartpole_run1.png "6_PPOKL_cartpole_run1")
![](report_images/6_PPOKL_cartpole_run2.png "6_PPOKL_cartpole_run2")\
Malgré ces remarques, PPO semble quand même plus stable que A2C.

**LunarLander**\
**PPO KL** Sur lunarLander on constate que la réglarisation KL permet de converger vers une rsum plus élevée que A2C seul:\
![](report_images/6_PPOKL_lunarlander.png "6_PPOKL_lunarlander")\
Avec **PPO Clipped** je n'ai pas réussi à trouver de paramètres qui permettent de dépasser A2C mais on parvient tout de même à l'égaliser.\
![](report_images/6_PPOclipped_lunarlander.png "6_PPOclipped_lunarlander")


## 7 - DDPG
**MountainCar**\
On converge très facillement avec DDPG sur MountainCar. Dès le deuxième épisode, l'agent est capable de résoudre le problème et en seulement une trentaine d'episodes on converge :\
![](report_images/7_DDPG_mountaincar.png "7_DDPG_mountaincar")

**LunarLander**\
De même avec lunarLander, on converge en un milliers d'épisodes environ, malgré quelques fails de temps à autres.\
![](report_images/7_DDPG_lunarlander.png "7_DDPG_lunarlander")

**Pendulum**\
Sur pendulum, il faut environ 30 itérations à DDPG pour converger:\
![](report_images/7_DDPG_pendulum.png "7_DDPG_pendulum")



## 8 - MADDPG
J'ai implémenté la version de base de MADDPG et sa version avec plusieurs sous-politiques par agent.

**simple_spread**\
Ci-dessous on observe les rewards cumulés, en **orange**  la version de base et en **vert** la version avec 5 sous-politiques par agent.\
![](report_images/8_MADDPG_spread_rsum.png "8_MADDPG_spread_rsum")

On constate que les agents apprennent, dans une certaine mesure. L'apprentissage est très lent.
Le plus étonnant est surtout qu'il y ai si peu de différence entre les deux versions.
Ci-dessous les loss des critiques et des actors de chacuns des agents dans les deux versions.
On constate que toutes les loss sont confondues et on ne parvient pas vraiement à distinguer l'impact de l'ajout de sous-politiques.\
![](report_images/8_MADDPG_spread_loss_Q.png "8_MADDPG_spread_loss_Q")
![](report_images/8_MADDPG_spread_loss_mu.png "8_MADDPG_spread_loss_mu")

## 9 - GAN - with Faces
![](report_images/9_GAN_faces_0.png "9_GAN_faces_0")
![](report_images/9_GAN_faces_1.png "9_GAN_faces_1")\
![](report_images/9_GAN_faces_15.png "9_GAN_faces_15")
![](report_images/9_GAN_faces_31.png "9_GAN_faces_31")\
En haut à gauche un échantillon des sorties du générateur non entraîné, en haut a droite après seulement 500 batchs, en bas à gauche à mi-temps de l'entraînement et en bas à droite les sorties finales.
D'autres sorties intermédiaires sont disponibles dans TME9_RLD_done.html.


Résultats obtenus après environ 1h30 d'entrarînement sur GPU sur colab

Le discriminateur avait tendance à être strictement meilleur que le générateur.
La seule heuristique que j'ai choisi d'ajouter pour remédier à ce problème, par rapport à un entraînement GAN "standard", est de n'entraîner le discriminateur que lorsque le générateur a obtenu (à l'epoch précédante) une précision supérieure à un seuil prédéfini (choisi à 0.2). J'ai aussi essayé d'autres heuristiques comme une maj "soft" des paramètres du discriminateur (theta = tau\*new_theta + (1-tau)\*theta) mais sans succès.

Ci-dessous les courbes d'apprentissage. En **rouge** et **bleu foncé** les accuracy et loss du discriminateur. En **rose** et **bleu ciel** les accuracy et loss du générateur.\
![](report_images/9_GAN_accuracy.png "9_GAN_accuracy")
![](report_images/9_GAN_loss.png "9_GAN_loss")

## 10 - VAE - on MNIST
En **gris** on observe les courbes d'apprentissage (- loglikelihood,  pénalité kl et loss) pour un état latent de taille 2 (`mu.shape=(2,)` et  `logsigma.shape=(2,)`). En **vert** un état latent de taille 10 et en **rouge** une taille 20. Ces 3 jeux de courbes ont été obtenus avec en entrée de l'encodeur des images en **niveaux de gris**.

En regardant les images, on constate que le résultat obtenu est très satisfaisant pour la taille 20 et relativement bien pour la taille 10 mais assez approximatif pour la taille 2.

Les courbes **orange**, **violet** et **bleu** montrent les résultats des mêmes expériences mais avec des images d'entrée **binaires** (avec un seuil à 0.5). On ne constate pas de différence entre les résultats avec entrée binaire et avec entrée en niveaux de gris.

![](report_images/10_VAE_loglikelihood.png "10_VAE_-loglikelihood")
![](report_images/10_VAE_kl_penalty.png "10_VAE_kl_penalty")

![](report_images/10_VAE_loss.png "10_VAE_loss")

![](report_images/10_VAE_image_0.png)![](report_images/10_VAE_image_1.png)
![](report_images/10_VAE_image_2.png)![](report_images/10_VAE_image_3.png)
![](report_images/10_VAE_image_4.png)![](report_images/10_VAE_image_5.png)
![](report_images/10_VAE_image_6.png)![](report_images/10_VAE_image_7.png)
![](report_images/10_VAE_image_8.png)![](report_images/10_VAE_image_9.png)
