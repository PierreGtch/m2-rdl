# Reinforcement Learning Algorithms

This repository contains implementations and comparisons of some
reinforcement learning algorithms.

It has been made during the reinforcement learning course of the DAC Master at Sorbonne University by Sylvain Lamprier.

http://dac.lip6.fr/master/rladl/

report.pdf describes all the important results

 - 1 - **Bandits** (tme1.ipynb)
 - 2 - **Value Iteration, Policy Iteration** on GridWorld (tme_gym/tme2.ipynb)
 - 3 - **Q-learning, SARSA, DynaQ** on GridWorld  (tme_gym/tme3.ipynb)
 - 4 - **DQN**  on GridWorld, CartPole and LunarLander (tme_gym/tme4.ipynb)
 - 5 - **A2C**: online and batched on GridWorld, CartPole and LunarLander  (tme_gym/tme5.ipynb)
 - 6 - **PPO**: KL and Clipped  on GridWorld, CartPole and LunarLander  (tme_gym/tme6.ipynb)
 - 7 - **DDPG**  on MountainCar, Pendulum and LunarLander(tme_gym/tme7.ipynb)
 - 8 - **MADDPG** adversarial and cooperative tasks (tme_gym/tme8.ipynb)
 - 9 - **GAN** on faces (TME9_RLD_done.ipynb)
 - 10 - **VAE** on MNIST (TME10.ipynb)

From 2 to 8, algorithms are implemented as *agents* playing on different `gym` environments.
All agents derive from the main `BaseAgent` class defined in tme_gym/utils_rdl.py.
This makes the algorithms easier to read and allows us use a single function (`tme_gym.utils_rdl.train_agent`) to train every agents on every environment.
